package manejocoleccionesgenericas;

import java.util.*;
/**
 *
 * @author tinix
 */
public class ManejoColeccionesGenericas {

    public static void main(String[] args) {
        List<String> miLista = new ArrayList<>();
        miLista.add("1");
        miLista.add("2");
        miLista.add("3");
        miLista.add("4");
        miLista.add("4");
        imprimir(miLista);
        
        Set<String> miSet = new HashSet<>();
        miSet.add("1000");
        miSet.add("2000");
        miSet.add("3000");
        miSet.add("3000");
        imprimir(miSet);
        
        Map<String, String> miMapa = new HashMap<>();
        miMapa.put("1", "Juan");
        miMapa.put("2","George");
        miMapa.put("3", "Rosario");
        miMapa.put("4", "Esperanza");
        imprimir(miMapa.keySet());
        imprimir(miMapa.values());
        
    }
    
    static void imprimir(Collection<String> col) {
        for(String elemento : col) {
            System.out.println(elemento + ""); 
        }
        
        System.out.println("");
    }
    
}
